package com.epam.helper;

public class Helper {
    public static final String MENU = "Press 1 to show string container\n" +
            "Press 2 to show europe countries list\n" +
            "Press 3 to show string container";
    public static final String WRONG_INPUT = "Wrong input. Enter an integer from 1 to 4";
    public static final String COUNTRIES_ARAY = "Array of countries: ";
    public static final String COUNTRIES_LIST = "List of countries: ";
    public static final String SORTED_COUNTRIES_LIST = "Sorted list of countries: ";
    public static final String SORTED_COUNTRIES_ARRAY = "Sorted array of countries: ";
    public static final String STRING_CONTAINER = "Container of strings ";
    public static final String GERMANY = "Germany";
    public static final String SPAIN = "Spain";
    public static final String ITALY = "Italy";
    public static final String BERLIN = "Berlin";
    public static final String ROME = "Rome";
    public static final String MADRID = "Madrid";
    public static final String COUNTRY = "Country: ";
    public static final String CAPITAL = "Capital: ";
    public static final String YOUR_DEQUE = "Your deque: ";
    public static final String ENTER_ELEMENT = "Enter element: ";

}
