package com.epam.model.deque;

public interface Deque<T> {
    void addFirst(T t);

    void addLast(T t);

    boolean offerFirst(T t);

    boolean offerLast(T t);

    T peekFirst();

    T peekLast();

    T removeFirst();

    T removeLast();

    T pollFirst();

    T pollLast();

    T getFirst();

    T getLast();

    T[] getElements();
}
