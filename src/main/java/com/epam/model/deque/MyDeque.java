package com.epam.model.deque;

import java.util.NoSuchElementException;

public class MyDeque<T> implements Deque<T> {

    private Object[] elements;
    private int firstElement;
    private int lastElement;

    public MyDeque() {
        elements = new Object[2];
        firstElement = 0;
        lastElement = 0;
    }

    @Override
    public T[] getElements() {
        Object[] tempArray = new Object[lastElement - firstElement];
        System.arraycopy(elements, 0, tempArray, 0, lastElement);
        return (T[]) tempArray;
    }

    @Override
    public void addFirst(T t) {
        if (t == null) {
            throw new NullPointerException();
        }
        if ((lastElement + 1) >= elements.length) {
            resize();
            addToBeginning(t);
        } else {
            addToBeginning(t);
        }
    }

    private void addToBeginning(T t) {
        Object[] tempArray = new Object[elements.length];
        System.arraycopy(elements, 0, tempArray, 1, lastElement);
        tempArray[firstElement] = t;
        elements = tempArray;
        lastElement++;
    }

    private void resize() {
        int newCapacity = elements.length * 2;
        Object[] tempArray = new Object[newCapacity];
        System.arraycopy(elements, 0, tempArray, 0, elements.length);
        elements = tempArray;
    }

    @Override
    public void addLast(T t) {
        if (t == null) {
            throw new NullPointerException();
        }
        if ((lastElement + 1) >= elements.length) {
            resize();
            addToEnd(t);
        } else {
            addToEnd(t);
        }
    }

    private void addToEnd(T t) {
        Object[] tempArray = new Object[elements.length];
        System.arraycopy(elements, 0, tempArray, 0, lastElement);
        tempArray[lastElement] = t;
        elements = tempArray;
        lastElement++;
    }

    @Override
    public boolean offerFirst(T t) {
        addFirst(t);
        return true;
    }

    @Override
    public boolean offerLast(T t) {
        addLast(t);
        return true;
    }

    @Override
    public T peekFirst() {
        if (elements.length == 0) {
            return null;
        } else {
            return (T) elements[firstElement];
        }
    }

    @Override
    public T peekLast() {
        if (elements.length == 0) {
            return null;
        } else {
            return (T) elements[elements.length - 1];
        }
    }

    @Override
    public T removeFirst() {
        T elementToRemove = pollFirst();
        if (elementToRemove == null) {
            throw new NoSuchElementException();
        } else {
            return elementToRemove;
        }
    }

    @Override
    public T removeLast() {
        T elementToRemove = pollLast();
        if (elementToRemove == null) {
            throw new NoSuchElementException();
        } else {
            return elementToRemove;
        }
    }

    @Override
    public T pollFirst() {
        if (elements.length != 0) {
            T elementToPoll = (T) elements[firstElement];
            Object[] tempArray = new Object[elements.length];
            System.arraycopy(elements, 1, tempArray, 0, lastElement);
            elements = tempArray;
            lastElement--;
            return elementToPoll;
        } else {
            return null;
        }
    }

    @Override
    public T pollLast() {
        if (elements.length != 0) {
            lastElement--;
            T elementToRemove = (T) elements[lastElement];
            elements[lastElement] = null;
            return elementToRemove;
        } else {
            return null;
        }
    }

    @Override
    public T getFirst() {
        T resultElement = (T) elements[firstElement];
        if (resultElement == null) {
            throw new NoSuchElementException();
        } else {
            return resultElement;
        }
    }

    @Override
    public T getLast() {
        T resultElement = (T) elements[elements.length - 1];
        if (resultElement == null) {
            throw new NoSuchElementException();
        } else {
            return resultElement;
        }
    }
}
