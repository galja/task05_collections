package com.epam.model.binary_tree;

public class Node<K, V> implements Comparable {
    private K key;
    private V value;
    private Node left;
    private Node right;

    public Node(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public Node getLeft() {
        return left;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public Node getRight() {
        return right;
    }

    public void setRight(Node right) {
        this.right = right;
    }

    public K getKey() {
        return key;
    }

    public void setKey(K key) {
        this.key = key;
    }

    public V getValue() {
        return value;
    }

//    public void setValue(V value) {
//        this.value = value;
//    }

    @Override
    public int compareTo(Object o) {
        return this.key.toString().compareTo(o.toString());
    }

    @Override
    public String toString() {
        return "Key: " + getKey() + "\tValue: " + getValue();
    }
}
