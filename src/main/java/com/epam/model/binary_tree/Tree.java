package com.epam.model.binary_tree;

public interface Tree<K, V> {
    void insert(K key, V value);

    void remove(K key);

    String treeToString();

    Node search(K key);
}
