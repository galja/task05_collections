package com.epam.model.binary_tree;

public class BinaryTree<K, V> implements Tree<K,V>{

    private Node root;
    private String stringToPrint;

    public Node getRoot() {
        return this.root;
    }

    @Override
    public void insert(K key, V value) {
        if (root == null) {
            this.root = new Node(key, value);
        } else {
            insertNode(this.root, key, value);
        }
    }
    @SuppressWarnings("unchecked")
    private Node insertNode(Node node, K key, V value) {
        if (node == null) {
            node = new Node(key, value);
        } else {
            if (key.toString().compareTo(node.getKey().toString()) < 0) {
                node.setLeft(insertNode(node.getLeft(), key, value));
            } else if (key.toString().compareTo(node.getKey().toString()) > 0) {
                node.setRight(insertNode(node.getRight(), key, value));
            } else {
                node = insertNode(node, key, value);
            }
        }
        return node;
    }

    @Override
    public void remove(K key) {

        removeNode(this.root, key);
    }

    private Node removeNode(Node root, K key) {

        if (root == null) return root;

        if (key.toString().compareTo(root.getKey().toString()) < 0) {
            root.setLeft(removeNode(root.getLeft(), key));
        } else if (key.toString().compareTo(root.getKey().toString()) > 0) {
            root.setRight(removeNode(root.getRight(), key));
        } else {
            if (root.getLeft() == null && root.getRight() == null) {
                return null;
            } else if (root.getLeft() == null) {
                return root.getRight();
            } else if (root.getRight() == null) {
                return root.getLeft();
            } else {
                K minKey = minKey(root.getRight());
                root.setKey(minKey);
                root.setRight(removeNode(root.getRight(), minKey));
            }
        }

        return root;
    }

    private K minKey(Node node) {
        if (node.getLeft() != null) {
            return minKey(node.getLeft());
        }
        return (K) node.getKey();
    }

    @Override
    public String treeToString() {
        stringToPrint = new String();
        traverceTreeInorder(this.root);
        return stringToPrint;
    }

    private void traverceTreeInorder(Node root) {
        if (root != null) {
            traverceTreeInorder(root.getLeft());
            stringToPrint += root.toString() + "\n";
            traverceTreeInorder(root.getRight());
        }
    }

    Node search(Node node, K key) {
        if (node == null || node.getKey() == key)
            return node;
        if (key.toString().compareTo(node.getKey().toString()) < 0)
            return search(node.getLeft(), key);
        else
            return search(node.getRight(), key);
    }

    @Override
    public Node search(K key) {
        return search(root, key);
    }


    //    public static void main(String a[]) {
//
//        BinaryTree<Integer, Integer> bst = new BinaryTree<Integer, Integer>();
//        bst.insert(8, 9);
//        bst.insert(10, 3);
//        bst.insert(14, 8);
//        bst.insert(3, 7);
//        bst.insert(6, 5);
//        bst.insert(7, 4);
//        System.out.println("-------------------");
//        System.out.println("In Order Traversal");
//
//        System.out.println(bst.treeToString());
//        System.out.println(bst.search(3).getValue());
//    }
}