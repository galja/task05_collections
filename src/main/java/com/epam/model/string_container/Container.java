package com.epam.model.string_container;

public interface Container {
    void add(String stringToAdd);
    String[] getStringArray();
}
