package com.epam.model.string_container;

import com.epam.helper.Helper;

public class StringGenerator {
    private StringContainer stringContainer;
    public StringGenerator(){
        stringContainer = new StringContainer();
        fillContainer();
    }

    public String[] getStringContainer() {
        return stringContainer.getStringArray();
    }

    private void fillContainer(){
        stringContainer.add(Helper.GERMANY);
        stringContainer.add(Helper.ITALY);
    }
    public void addToContainer(String string){
        stringContainer.add(string);
    }
}
