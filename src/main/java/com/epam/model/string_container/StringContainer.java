package com.epam.model.string_container;

import java.util.Arrays;

public class StringContainer implements Container{
    private String[] stringArray;

    public StringContainer() {
        stringArray = new String[0];
    }

    public String[] getStringArray() {
        return stringArray;
    }

    @Override
    public void add(String stringToAdd) {
        String[] tempArray = Arrays.copyOf(stringArray, stringArray.length);
        stringArray = new String[stringArray.length + 1];
        for (int i = 0; i < stringArray.length; i++) {
            if (i == stringArray.length - 1) {
                this.stringArray[i] = stringToAdd;
            } else {
                this.stringArray[i] = tempArray[i];
            }
        }
    }
}

