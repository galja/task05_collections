package com.epam.model.comparator;

import com.epam.helper.Helper;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CountriesGenerator {

    private EuropeCountry[] countriesArray;
    private List<EuropeCountry> countriesList;

    private EuropeCountry spain;
    private EuropeCountry germany;
    private EuropeCountry italy;

    public CountriesGenerator() {
        countriesArray = new EuropeCountry[3];
        countriesList = new ArrayList<>();
        germany = new EuropeCountry(Helper.GERMANY, Helper.BERLIN);
        italy = new EuropeCountry(Helper.ITALY, Helper.ROME);
        spain = new EuropeCountry(Helper.SPAIN, Helper.MADRID);
        generateArrays();
    }

    public EuropeCountry[] getCountriesArray() {
        return countriesArray;
    }

    public List<EuropeCountry> getCountriesList() {
        return countriesList;
    }

    public EuropeCountry[] getSortedCountriesArray(){
        Arrays.sort(countriesArray);
        return countriesArray;
    }
    public List<EuropeCountry> getSortedCountriesList() {
        Collections.sort(countriesList);
        return countriesList;
    }

    private void generateArrays() {
        countriesArray[0] = spain;
        countriesArray[1] = germany;
        countriesArray[2] = italy;
        countriesList.add(spain);
        countriesList.add(germany);
        countriesList.add(italy);
    }

    public String toString() {
        return Arrays.toString(countriesArray);
    }
}
