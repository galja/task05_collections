package com.epam.model.comparator;

import com.epam.helper.Helper;

public class EuropeCountry implements Comparable<EuropeCountry> {
    private String countryName;
    private String capital;

    public String getCountryName() {
        return countryName;
    }

    public String getCapital() {
        return capital;
    }

    public EuropeCountry(String countryName, String capital) {
        this.countryName = countryName;
        this.capital = capital;
    }

    @Override
    public int compareTo(EuropeCountry anotherEuropeCountry) {
        return this.countryName.compareTo(anotherEuropeCountry.getCountryName());
    }

    @Override
    public String toString() {
        return Helper.COUNTRY + this.countryName + "\t" + Helper.CAPITAL + this.capital;
    }
}
