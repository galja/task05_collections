package com.epam.model.comparator;

import java.util.Comparator;

public class CountriesComparator implements Comparator<EuropeCountry> {
    @Override
    public int compare(EuropeCountry o1, EuropeCountry o2) {
        return o1.getCapital().compareTo(o2.getCapital());
    }
}
