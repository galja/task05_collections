package com.epam.controller;

import com.epam.model.binary_tree.*;
import com.epam.model.comparator.CountriesGenerator;
import com.epam.model.deque.*;
import com.epam.model.string_container.*;

public class Controller {
    private CountriesGenerator countriesGenerator;
    private StringGenerator stringGenerator;
    private Tree<String, String> binaryTree;
    private Deque<String> deque;

    public Controller() {
        countriesGenerator = new CountriesGenerator();
        stringGenerator = new StringGenerator();
        binaryTree = new BinaryTree();
        deque = new MyDeque<>();
    }

    public Deque getDeque(){return deque;}

    public CountriesGenerator getCountriesGenerator() {
        return countriesGenerator;
    }

    public StringGenerator getStringGenerator() {
        return stringGenerator;
    }

    public Tree getBinaryTree() {
        return binaryTree;
    }
}
