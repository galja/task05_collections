package com.epam.controller;

import com.epam.model.deque.Deque;
import com.epam.model.deque.MyDeque;

public class DequeController {
    Deque<Object> deque;

    public DequeController(){
        deque = new MyDeque();
    }

    public void addFirstElement(Object value){
        deque.addFirst(value);
    }
    public void addLastElement(Object value){
        deque.addLast(value);
    }
    public void removeFirstElement(){
        deque.removeFirst();
    }
    public void removeLastElement(){
        deque.removeLast();
    }
    public Object[] printDeque(){
        return deque.getElements();
    }
}
