package com.epam.view;

public interface Printable {
    void print();
}