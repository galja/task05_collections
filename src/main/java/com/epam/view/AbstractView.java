package com.epam.view;

import com.epam.controller.Controller;
import com.epam.model.comparator.EuropeCountry;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.List;

public abstract class AbstractView {
    private static Logger logger = LogManager.getLogger(AbstractView.class);
    private Controller controller;

    public AbstractView() {
        controller = new Controller();
    }

    public void print(List<EuropeCountry> array, String message) {
        logger.trace(message);
        for (Object element : array) {
            logger.info(element);
        }
    }

    public void print(Object[] array, String message) {
        logger.trace(message);
        String str="";
        for (Object element : array) {
            str+=element+"\n";
        }
        logger.info(str);
    }
}
