package com.epam.view;

import com.epam.controller.Controller;
import com.epam.helper.Helper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class ConsoleView extends AbstractView {
    private static Logger logger;
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private Scanner input;

    public ConsoleView() {
        logger = LogManager.getLogger(ConsoleView.class);
        controller = new Controller();
        input = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - String container");
        menu.put("2", "  2 - Comparator");
        menu.put("3", "  3 - Deque");
        menu.put("4", "  4 - Binary tree");
        menu.put("Q", "  Q - exit");
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::containerMenu);
        methodsMenu.put("2", this::comparatorMenu);
        methodsMenu.put("3", this::dequeMenu);
        methodsMenu.put("4", this::treeMenu);
    }

    private void containerMenu() {
        Map<String, String> containerMenu = new LinkedHashMap<>();
        containerMenu.put("1", " 1 - print Countries from container");
        containerMenu.put("2", " 2 - add Country to container");
        Map<String, Printable> containerMethodsMenu = new LinkedHashMap<>();
        containerMethodsMenu.put("1", this::showContainer);
        containerMethodsMenu.put("2", this::addCountryToContainer);
        outputMenu(containerMenu);
        String keyMenu = input.nextLine().toUpperCase();
        containerMethodsMenu.get(keyMenu).print();
    }

    private void comparatorMenu() {
        Map<String, String> comparatorMenu = new LinkedHashMap<>();

        comparatorMenu.put("1", " 1 - print coutries array and sort it by capital ");
        comparatorMenu.put("2", " 2 - print coutries list and sort it by capital");
        Map<String, Printable> comparatorMethodsMenu = new LinkedHashMap<>();
        comparatorMethodsMenu.put("1", this::showArrayComparator);
        comparatorMethodsMenu.put("2", this::showListComparator);
        outputMenu(comparatorMenu);
        String keyMenu = input.nextLine().toUpperCase();
        comparatorMethodsMenu.get(keyMenu).print();
    }

    private void treeMenu() {
        Map<String, String> treeMenu = new LinkedHashMap<>();
        treeMenu.put("1", " 1 - add node ");
        treeMenu.put("2", " 2 - remove node");
        treeMenu.put("3", " 3 - print tree");
        treeMenu.put("4", " 4 - search node");
        Map<String, Printable> treeMethodsMenu = new LinkedHashMap<>();
        treeMethodsMenu.put("1", this::addNodeToTree);
        treeMethodsMenu.put("2", this::removeNodeFromTree);
        treeMethodsMenu.put("3", this::printTree);
        treeMethodsMenu.put("4", this::findNode);
        outputMenu(treeMenu);
        String keyMenu = input.nextLine().toUpperCase();
        treeMethodsMenu.get(keyMenu).print();
    }

    private void dequeMenu() {
        Map<String, String> dequeMenu = new LinkedHashMap<>();
        dequeMenu.put("1", " 1 - add first element to deque");
        dequeMenu.put("2", " 2 - remove element to deque");
        dequeMenu.put("3", " 3 - print deque to deque");
        Map<String, Printable> dequeMethodsMenu = new LinkedHashMap<>();
        dequeMethodsMenu.put("1", this::addFirstElementToDeque);
        dequeMethodsMenu.put("2", this::addLastElementToDeque);
        dequeMethodsMenu.put("3", this::removeFirstElementFromDeque);
        dequeMethodsMenu.put("4", this::removeLastElementFromDeque);
        outputMenu(dequeMenu);
        String keyMenu = input.nextLine().toUpperCase();
        dequeMethodsMenu.get(keyMenu).print();
    }

    private void addFirstElementToDeque() {
        controller.getDeque().addFirst(scanLine(Helper.ENTER_ELEMENT));
        print(controller.getDeque().getElements(), Helper.YOUR_DEQUE);
    }

    private void addLastElementToDeque() {
        controller.getDeque().addLast(scanLine(Helper.ENTER_ELEMENT));
        print(controller.getDeque().getElements(), Helper.YOUR_DEQUE);
    }

    private void removeFirstElementFromDeque() {
        controller.getDeque().removeFirst();
        print(controller.getDeque().getElements(), Helper.YOUR_DEQUE);
    }

    private void removeLastElementFromDeque() {
        controller.getDeque().removeLast();
        print(controller.getDeque().getElements(), Helper.YOUR_DEQUE);
    }

    private void findNode() {
        logger.info("Tree: \n");
        printTree();
        logger.info(controller.getBinaryTree().search(scanLine("Enter key: ")).toString());

    }

    private void addNodeToTree() {
        String key = scanLine("Enter key: ");
        String value = scanLine("Enter value: ");

        controller.getBinaryTree().insert(key, value);
        printTree();
    }

    private void removeNodeFromTree() {
        logger.info("Tree before removing: \n");
        printTree();
        controller.getBinaryTree().remove(scanLine("Enter key: "));
        logger.info("Tree before removing: \n");
        printTree();
    }

    private void printTree() {
        logger.info(controller.getBinaryTree().treeToString());
    }

    private void showArrayComparator() {
        print(controller.getCountriesGenerator().getCountriesArray(), "Array: ");
        print(controller.getCountriesGenerator().getSortedCountriesArray(), "Array sorted by capital: ");
    }

    private void showListComparator() {
        print(controller.getCountriesGenerator().getCountriesList(), "List: ");
        print(controller.getCountriesGenerator().getSortedCountriesArray(), "List sorted by capital: ");
    }

    private void showContainer() {
        print(controller.getStringGenerator().getStringContainer(), Helper.STRING_CONTAINER);
    }

    private void addCountryToContainer() {
        controller.getStringGenerator().addToContainer(scanLine("Enter country name"));
    }

    private void outputMenu(Map menu) {
        System.out.println("\nMENU:");
        for (Object str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu(menu);
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            methodsMenu.get(keyMenu).print();
        } while (!keyMenu.equals("Q"));
    }

    private String scanLine(String message) {
        logger.info(message);
        return input.nextLine().toUpperCase();
    }
}
